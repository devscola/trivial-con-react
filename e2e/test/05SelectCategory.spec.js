import { SelectCategory } from '../pageObjects/selectCategory.js'
import { Identifyer } from '../pageObjects/identifyer.js'

describe('Trivial: SelectCategory', () => {
    let selectCategory = new SelectCategory()
    let identifyer = new Identifyer()

    beforeEach(() => {
        cy.visit('/')
        cy.wait(200)
    })

    it('check if player can access a game', () => {
        identifyer.accessGame()
        cy.wait(200)
        selectCategory.clickJoinGame()
        cy.wait(200)
        selectCategory.selectCategory()
        selectCategory.selectGame('Trivial Mania')

        cy.get('#root')
            .contains('Time left')
    })

    it('check if can see an empty category', () => {
        identifyer.accessGame()
        cy.wait(200)
        selectCategory.clickJoinGame()
        cy.wait(200)
        selectCategory.selectWrongCategory()

        cy.get('#root')
            .contains('No hay juegos')
    })
})