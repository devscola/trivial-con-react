import { BuildNewGame } from '../pageObjects/buildNewGame'
import { Identifyer } from '../pageObjects/identifyer'
import { AddGameInfo } from '../pageObjects/addGameInfo'

describe('Trivial:Build New Game', () => {
    let buildNewGame = new BuildNewGame()
    let identifyer = new Identifyer()
    let addGameInfo = new AddGameInfo()
    
    beforeEach(() => {
        buildNewGame.visitOurHomePage()
        buildNewGame.loadPage()
    })

    it('allows users to enter the "send questions" option', () => {
        identifyer.accessGame()
        cy.wait(500)
        addGameInfo.createGame('Trivial Game II')
        cy.wait(600)
        cy.get("#root").contains('Enter questions')
    })

    it('Users sends a question and three answers', () => {
        identifyer.accessGame()
        buildNewGame.loadPage()
        addGameInfo.createGame('Trivial Game III')
        buildNewGame.loadPage()
        buildNewGame.writeAQuestion()
        buildNewGame.writeFirstAnswer()
        buildNewGame.writeSecondAnswer()
        buildNewGame.writeThirdAnswer()
        buildNewGame.checkCorrectAnswer()
        buildNewGame.sendQuestion()
    })
})