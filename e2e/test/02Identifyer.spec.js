import { Identifyer } from '../pageObjects/identifyer.js'

describe('identifyer', () => {

  let identifyer = new Identifyer()
 
  beforeEach(() => {
    cy.visit('/')
    cy.wait(200)

  })

  it('allows user to introduce their ID', () => {
    identifyer.writeUserName()
    identifyer.writePassword()
    identifyer.clickSignInButton()
    cy.wait(200)
    cy.get("#root").contains('Hello')
  })

  it('shows a modal if no name was written', () => {
    identifyer.clickSignInButton()
    cy.get("div").contains('You need a user name')
  })
})