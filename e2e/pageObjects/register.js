const REGISTER_BUTTON = 'Register'
const ID_INPUT = '[placeholder="username"]'
const USER_NAME = "El Sagan"
const PASSWORD = '123456'
const PASSWORD_INPUT = '[placeholder="password"]'
const REPEAT_PASSWORD_INPUT = '[placeholder="repeat password"]'
const CREATE_USER_BUTTON = 'Create new user'
const BACK_BUTTON = 'Back'

class Register {

  clickRegisterButton(){
    cy.get("button")
    .contains(REGISTER_BUTTON)
    .click()
  }

  writeUserName(){
    cy.get(ID_INPUT).type(USER_NAME)
  }


  writePassword(){
    cy.get(PASSWORD_INPUT).type(PASSWORD)
  }

  writePasswordConfirmation(){
    cy.get(REPEAT_PASSWORD_INPUT).type(PASSWORD)
  }

  clickCreateNewUserButton(){
    cy.get("button")
    .contains(CREATE_USER_BUTTON)
    .click()
  }

  clickBackButton(){
    cy.get("button")
    .contains(BACK_BUTTON)
    .click()
  }

  createNewUser(){
    this.clickRegisterButton()
    this.writeUserName()
    this.writePassword()
    this.writePasswordConfirmation()
    this.clickCreateNewUserButton()
  }

  
}

module.exports = { Register }
