const LOAD_TIME = 200
const QUESTION = "Funciona?"
const QUESTION_INPUT = '[placeholder="enter"]'
const FIRST_ANSWER = "YES"
const SECOND_ANSWER = "NO"
const THIRD_ANSWER = "Maybe"
const FOURTH_ANSWER = " Sometimes"
const FIFTH_ANSWER = " Never"
const CORRECT_ANSWER= '1'


class BuildNewGame {

  sendAQuestion(){
    this.writeAQuestion()
    this.writeFirstAnswer()
    this.writeSecondAnswer()
    this.writeThirdAnswer()
    this.checkCorrectAnswer()
    this.sendQuestion()
  }

  visitOurHomePage() {
    const url = 'localhost:1234'
    cy.visit(url)
  }

  loadPage() {
    cy.wait(LOAD_TIME)
  }

  writeAQuestion() {
    cy.get(QUESTION_INPUT).type(QUESTION)
  }

  writeFirstAnswer() {
    cy.get('[placeholder="answers"]')
      .type(FIRST_ANSWER)
    cy.wait(LOAD_TIME)
  }
  writeSecondAnswer() {
    cy.get("[value=Add]").click()
    cy.wait(LOAD_TIME)
    cy.get('[placeholder="answers"]')
      .last()
      .type(SECOND_ANSWER)
  }
  writeThirdAnswer() {
    cy.get("[value=Add]").click()
    cy.get('[placeholder="answers"]')
      .last()
      .type(THIRD_ANSWER)
  }

  writeFourthAnswer() {
    cy.get("[value=Add]").click()
    cy.get('[placeholder="answers"]')
      .last()
      .type(FOURTH_ANSWER)
  }

  writeFifthAnswer() {
    cy.get("[value=Add]").click()
    cy.get('[placeholder="answers"]')
      .last()
      .type(FIFTH_ANSWER)
  }

  checkCorrectAnswer() {
    cy.get('[type="radio"]').check(CORRECT_ANSWER)
  }

  sendQuestion() {
    cy.get("button")
      .contains('Send question')
      .click()
  }

  returnToMainMenu() {
    cy.get("button")
      .contains('Done')
      .click()
  }

  clickJoinGame() {
    cy.get("button")
      .contains('Join Game')
      .click()
  }

  checkExistingQuestion() {
    cy.get("#root")
      .contains(QUESTION)
  }

  writeDuplicateAnswer() {
    cy.get("[value=Add]").click()
    cy.get('[placeholder="answers"]')
      .last()
      .type(THIRD_ANSWER)
  }

}

module.exports = { BuildNewGame }