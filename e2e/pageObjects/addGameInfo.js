class AddGameInfo {
  clickNewGame() {
    cy.get("button")
      .contains('Build New Game')
      .click()
  }

  clickCreateGame() {
    cy.get("button")
      .contains('Create')
      .click()
  }

  selectCategory() {
    cy.get("button")
      .contains('Devscola')
      .click()
  }

  typeGameName(gameName) {
    cy.get("[placeholder='Write the name here']").type(gameName)
  }

  createGame(gameName) {
    this.clickNewGame()
    this.typeGameName(gameName)
    this.selectCategory()
    this.clickCreateGame()
  }

}

module.exports = { AddGameInfo }