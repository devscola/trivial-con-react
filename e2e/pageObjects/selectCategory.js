const CORRECT_ANSWER = "NO"
const WRONG_ANSWER = "YES"

class SelectCategory {

  clickJoinGame() {
    cy.get("button")
      .contains('Join Game')
      .click()
  }


  selectCategory() {
    cy.get("button")
      .contains('Devscola')
      .click()
  }

  selectGame(gameName){
    cy.get('span')
      .contains(gameName)
      .click()
  }

  selectWrongCategory() {
    cy.get("button")
      .contains('Curiosities')
      .click()
  }

  accessGame(gameName){
    this.clickJoinGame()
    this.selectCategory()
    this.selectGame(gameName)
  }

}

module.exports = { SelectCategory }