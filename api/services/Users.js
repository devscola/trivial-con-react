const UsersRepository = require('../repositories/UsersDB')
const usersRepository = new UsersRepository()
const Security = require('./securityProvider')
const security = new Security()

class UserService {

    async registerNewUser(userInfo) {
        const newUser = {
            userName: userInfo.userName,
            password: security.encryptData(userInfo.password)
        }

        const existingUser = await usersRepository.findUserByName(userInfo.userName)
        const userExist = {
            status: "fail",
            message: "This username is already taken"
        }
        const correctRegister = {
            status: "success",
            message: null
        }

        if (existingUser) {
            return userExist
        }

        usersRepository.saveUser(newUser)
        return correctRegister
    }

    async login(userInfo) {
        const userName = userInfo.userName
        const password = userInfo.password
        const errorWrongUser = {
            messageError: "Wrong username"
        }
        const errorWrongPassword = {
            messageError: "Wrong password"
        }
        const existingUser = await usersRepository.findUserByName(userName)
        if (!existingUser) {
            return errorWrongUser
        }
        const bytes = security.decryptData(existingUser.password);
        const originalText = bytes.toString(CryptoJS.enc.Utf8);
        if (originalText != password) {
            return errorWrongPassword
        }
        return []
    }
}
module.exports = UserService