const CryptoJS = require("crypto-js");
const salt = process.env.SALT || "salt"

class Security{
    encryptData(dataToEncrypt) {
        return CryptoJS.AES.encrypt(dataToEncrypt, salt).toString()
    }

    decryptData(dataToDecrypt) {
        return CryptoJS.AES.decrypt(dataToDecrypt, salt)
    }
}
module.exports = Security