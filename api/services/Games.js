const GamesRepository = require('../repositories/GamesDB')

const gamesRepository = new GamesRepository()

class GameService {
    retrieveGames(category) {
        const result = gamesRepository.retrieveGames(category)
        return result
    }

    retrieveQuestions(gameName) {
        const result = gamesRepository.retrieveQuestions(gameName)
        return result
    }

    saveGame(gameInfo) {
        const result = gamesRepository.saveGame(gameInfo)
        return result
    }

    saveQuestion(question, name) {
        
        const result = gamesRepository.saveQuestion(question, name)
        return result
    }
}

module.exports = GameService