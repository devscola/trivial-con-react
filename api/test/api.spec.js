
const question = {
    question: "A, B, C?",
    answers: ["A", "B", "C"],
    correctAnswer: "A"
}

const points = {
    userName: "Punset",
    points: [0, 1]
}

const userAndPassword = {
    userName: "Punset",
    password: "password"
}


describe('Api', () => {
    it('post a question to /sendQuestions', () => {
        cy.request('POST', `localhost:8081/sendQuestions`, question)
            .then((response) => {
                expect(response.status).to.eq(200)
            })

    })

    it('gets the questions from /retrieveQuestions', () => {
        cy.request('localhost:8081/retrieveQuestions').then((response) => {
            expect(response.body[0].answers.length).to.be.at.least(2)
        })
    })

    it('post the points to /userPoints', () => {
        cy.request('POST', `localhost:8081/userPoints`, points)
            .then((response) => {
                expect(response.status).to.eq(200)
            })
    })

    it(' registers new players via endpoint /registerNewUser', () => {
        cy.request('POST', `localhost:8081/registerNewUser`, userAndPassword)
            .then((response) => {
                expect(response.status).to.eq(200)
            })
    })

    it(' it logs players in  via endpoint /login', () => {
        cy.request('POST', `localhost:8081/login`, userAndPassword)
            .then((response) => {
                expect(response.body).to.eql([])
            })
    })


})