const MongoClient = require('mongodb').MongoClient
const url = process.env.MONGO || "mongodb://mongo/trivial"

const ERROR_CANNOT_CONNECT = {
  "status": "error",
  "message": "Unable to communicate with database"
}

class UsersRepository {
  async findUserByName(name) {
    const client = await MongoClient.connect(url)
    if (!client) {
      return ERROR_CANNOT_CONNECT
    }
    const db = client.db("trivial")
    const user = db.collection("users").findOne({ userName: name })
    client.close()
    return user
  }

  async saveUser(newUser) {
    const client = await MongoClient.connect(url)
    const db = client.db("trivial")
    db.collection("users").insertOne(newUser, function (err, res) {
      if (err) throw err;
    })
    client.close()
  }
}

module.exports = UsersRepository