const MongoClient = require('mongodb').MongoClient
const url = process.env.MONGO || "mongodb://mongo/trivial"

const ERROR_CANNOT_CONNECT = {
  "status": "error",
  "message": "Unable to communicate with database"
}

const SUCCESSFUL_RESPONSE = {
  "status": "success",
  "data": null
}

class GamesRepository {

  async saveGame(gameInfo) {
    
    const client = await MongoClient.connect(url)
    if (!client) {
      return ERROR_CANNOT_CONNECT
    }
    const db = client.db("trivial")

    const existingGameName = await db.collection("games").findOne({ category: gameInfo.category,name: gameInfo.name })
   
    if(existingGameName){
      const failMessage = {
        "status": "fail",
        "data": { message:'El nombre del juego ya existe.'}
      }
      return failMessage
    }
    db.collection('games').updateOne({ category: gameInfo.category }, { $push: { name: gameInfo.name } }, { upsert: true })
    client.close()
    return SUCCESSFUL_RESPONSE

  }

  async retrieveGames(category) {
    const noGamesAvailable = {
      "status": "success",
      "data": [{name:['No hay juegos']}]
    }

    const client = await MongoClient.connect(url)
    if (!client) {
      return ERROR_CANNOT_CONNECT
    }
    const db = client.db("trivial")
    const games = await db.collection("games").find({ category: category }).toArray()
    if (games == '') {
      return noGamesAvailable
    }
    const availableGames = {
      "status": "success",
      "data": games
    }
    client.close()
    return availableGames
  }

  async retrieveQuestions(gameName) {


    const client = await MongoClient.connect(url)
    if (!client) {
      return ERROR_CANNOT_CONNECT
    }
    const db = client.db("trivial")
    const questions = await db.collection("questions").find({ name: gameName }).toArray()
    const gameQuestions = {
      "status": "success",
      "data": questions[0].question
    }
    client.close()
    return gameQuestions
  }

  async saveQuestion(question, name) {


    const client = await MongoClient.connect(url)
    if (!client) {
      return ERROR_CANNOT_CONNECT
    }
    const db = client.db("trivial")
    db.collection('questions').updateOne({ name: name }, { $push: { question } }, { upsert: true })
    client.close()
    return SUCCESSFUL_RESPONSE
  }
}

module.exports = GamesRepository