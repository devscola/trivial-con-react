import React from "react"
import { render} from "@testing-library/react"
import ScoreScreen from '../components/ScoreScreen.js'

describe("Score screen", () => {
    it("renders the score screen when the answer is right", () => {
      const { getByText} = render(<ScoreScreen chosenAnswer='sagan' correctAnswer='sagan' userName='punset' totalPoints='1' resultToAnswer='right'/>)
  
      getByText(/Trivial Game/i)
      getByText(/Your answer was sagan/i)
      getByText(/right/i)
      getByText(/punset your points are:1/i)
    })

    it("renders the score screen when the answer is wrong", () => {
      const { getByText} = render(<ScoreScreen chosenAnswer='amapola' correctAnswer='sagan' userName='punset' totalPoints='0' resultToAnswer='wrong'/>)
  
      getByText(/Trivial Game/i)
      getByText(/Your answer was amapola/i)
      getByText(/wrong/i)
      getByText(/The right answer was: sagan/i)
      getByText(/punset your points are:0/i)
    })
})