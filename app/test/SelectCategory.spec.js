import React from "react"
import { render, fireEvent } from "@testing-library/react"
import SelectCategory from "../components/JoinGame/SelectCategory.js"


describe('SelectCategory', () => {
  it('Renders the component', () => {
    const { getByText } = render(<SelectCategory />)
    getByText(/DevTrivia/i)
    getByText(/DevScola/i)
    getByText(/Back/i)
  })

  it("makes work the back button ", () => {
    const callback = jest.fn()
    const { getByText } = render(<SelectCategory changeView={callback} />)

    const button = getByText(/Back/i)
    fireEvent.click(button)

    expect(callback).toHaveBeenCalledWith("Home")
  })

it("calls a function when button 'Devscola' is clicked", () => {
  const callback = jest.fn()
  const { getByText } = render(<SelectCategory handleClick={callback} />)

  const button = getByText(/Devscola/i)
  
fireEvent.click
(button)

  expect(callback).toHaveBeenCalledWith("Devscola")
})
 })


