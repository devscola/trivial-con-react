import React from "react"
import { render, fireEvent } from "@testing-library/react"
import Identifyer from "../components/Identifyer.js"

describe("Identifyer", () => {
  it("allows players to introduce their ID", () => {
    const { queryAllByText} = render(<Identifyer />)

    queryAllByText(/Create a user name/i)
  })
})