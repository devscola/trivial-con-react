import React from "react"
import { render, fireEvent } from "@testing-library/react"
import BuildNewGame from "../components/BuildNewGame.js"

describe("Build new game", () => {
    it("renders the form", () => {
      const { getByText} = render(<BuildNewGame/>)
  
      getByText(/Enter questions/i)
      getByText(/Add/i)
      getByText(/Send question/i)
      getByText(/Done/i)
    })
    it("dispatches an event after clicking add button", () => {
      const { container} = render(<BuildNewGame/>)
      const buttonAdd = container.querySelector('[type="submit"]')
      const mock = jest.fn()

      container.addEventListener('click', () => {mock(event)})
      const event = new Event('click', { bubbles: true })
      buttonAdd.dispatchEvent(event)
      
      expect(mock).toHaveBeenCalled()

    })

    it("dispatches an event after clicking send question button", () => {
        const { container} = render(<BuildNewGame/>)
        const mock = jest.fn();
        const buttons = container.querySelectorAll('button')
        const sendQuestionButton = buttons[0]
        
        container.addEventListener('click', () => {mock(event)})
        const event = new Event('click', { bubbles: true })
        sendQuestionButton.dispatchEvent(event)
        
         expect(mock).toHaveBeenCalled();
    })

    it("dispatches an event after clicking done button", () => {
        const mock = jest.fn();
        const { container } = render(<BuildNewGame changeView={mock}/>)
        const buttons = container.querySelectorAll('button')
        const doneButton = buttons[1]
        
        fireEvent.click(doneButton)
    
        expect(mock).toHaveBeenCalled();
    })
    
    it("dispatches a modal error if you dont write a question", () => {
        const { container, getByText} = render(<BuildNewGame/>)
        const mock = jest.fn()
        const buttons = container.querySelectorAll('button')
        const sendQuestionButton = buttons[0]
        
        fireEvent.click(sendQuestionButton)
      
        getByText(/You need to write a question/i)

    })

    it("dispatches a modal error if you dont write an answer", () => {
        const { container, getByText} = render(<BuildNewGame/>)
        const inputs = container.querySelectorAll('input')
        const questionInput = inputs[0]
        const buttons = container.querySelectorAll('button')
        const sendQuestionButton = buttons[0]
        
        fireEvent.change(questionInput, { target: { value: 'First question?'} })
        fireEvent.click(sendQuestionButton)

        getByText(/You need at least two answers/i)
    })

    it("dispatches a modal error if you dont choose correct answer", () => {
        const { container, getByText} = render(<BuildNewGame/>)
        const questionInput = container.querySelector('input')
        const firstAnswerInput = container.querySelector('[placeholder="answers"]')
        const buttonAdd = container.querySelector('[type="submit"]')
        const sendQuestionButton = container.querySelector('button')
        
        fireEvent.change(questionInput, { target: { value: 'First question?'} })
        fireEvent.change(firstAnswerInput, { target: { value: 'one answer'} })
        fireEvent.click(buttonAdd)
        const answersInput = container.querySelectorAll('[placeholder="answers"]')
        const secondAnswerInput = answersInput[1]
        fireEvent.change(secondAnswerInput, { target: { value: 'second answer'} })
        fireEvent.click(sendQuestionButton)

        getByText(/You need to choose a correct answer/i)
    })

    it("dispatches a modal error if you write the same answer twice", () => {
        const { container, getByText} = render(<BuildNewGame/>)
        const questionInput = container.querySelector('input')
        const firstAnswerInput = container.querySelector('[placeholder="answers"]')
        const buttonAdd = container.querySelector('[type="submit"]')
        const sendQuestionButton = container.querySelector('button')
        
        fireEvent.change(questionInput, { target: { value: 'First question?'} })
        fireEvent.change(firstAnswerInput, { target: { value: 'one answer'} })
        fireEvent.click(buttonAdd)
        const secondAnswerInput = container.querySelectorAll('[placeholder="answers"]')[1]
        fireEvent.change(secondAnswerInput, { target: { value: 'one answer'} })
        fireEvent.click(sendQuestionButton)

        getByText(/Don't write duplicated answers/i)
    })    

    it("does not allow more than 4 answers ", () => {
        const { container, getByText} = render(<BuildNewGame/>)
        const questionInput = container.querySelector('input')
        const firstAnswerInput = container.querySelector('[placeholder="answers"]')
        const buttonAdd = container.querySelector('[type="submit"]')
        

        fireEvent.change(questionInput, { target: { value: 'First question?'} })
        fireEvent.change(firstAnswerInput, { target: { value: 'one answer'} })
        fireEvent.click(buttonAdd)

        const secondAnswerInput = container.querySelectorAll('[placeholder="answers"]')[1]
        fireEvent.change(secondAnswerInput, { target: { value: 'second answer'} })
        fireEvent.click(buttonAdd)
        
        const thirdAnswerInput = container.querySelectorAll('[placeholder="answers"]')[2]
        fireEvent.change(thirdAnswerInput, { target: { value: 'third answer'} })
        fireEvent.click(buttonAdd)

        const fourthAnswerInput = container.querySelectorAll('[placeholder="answers"]')[3]
        fireEvent.change(fourthAnswerInput, { target: { value: 'fourth answer'} })
        fireEvent.click(buttonAdd)
        

        getByText(/Error: The maximun number of answers is four/i)
    })    
})