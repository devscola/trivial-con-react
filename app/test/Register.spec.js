import React from "react"
import fetchMock from "fetch-mock"
import { render, fireEvent, waitFor } from "@testing-library/react"
import Register from "../components/Register.js"

describe("Register", () => {
  it("renders the registration form", () => {
    const { container } = render(<Register />)
    const inputs = container.querySelectorAll('input')
    const buttons = container.querySelectorAll('button')

    expect(inputs.length).toBe(3)
    expect(buttons.length).toBe(2)
  })

  it("calls a function when button back is clicked", () => {
    const callback = jest.fn()
    const { container } = render(<Register changeView={callback} />)
    const buttonBack = container.querySelectorAll('button')[1]

    fireEvent.click(buttonBack)
    expect(callback).toHaveBeenCalled()
  })

  it("succeeds to register a new user", async () => {
    const mockedResponse = []
    const config = {
      matcher: `${process.env.API_URL}/registerNewUser`,
      response: mockedResponse,
      method: "POST"
    }
    fetchMock.mock(config)
    const nameChanged = jest.fn()
    const changeView = jest.fn()
    const { container } = render(<Register nameChanged={nameChanged} changeView={changeView} />)
    const userNameInput = container.querySelector('[placeholder="username"]')
    const passwordInput = container.querySelector('[placeholder="password"]')
    const repeatPasswordInput = container.querySelector('[placeholder="repeat password"]')
    const buttonCreateNewUser= container.querySelectorAll('button')[0]

    fireEvent.change(userNameInput, { target: { value: 'Punset'} })
    fireEvent.change(passwordInput, { target: { value: 'Punset'} })
    fireEvent.change(repeatPasswordInput, { target: { value: 'Punset'} })
    await waitFor(() => fireEvent.click(buttonCreateNewUser))

    expect(nameChanged).toHaveBeenCalledWith('Punset')
    expect(changeView).toHaveBeenCalledWith('Home')
    fetchMock.restore();
  })
})