import React from "react"
import { render, act } from "@testing-library/react"
import Timer from "../components/Timer"

describe("Timer", () => {
  afterEach(() => {
    jest.clearAllMocks()
  })

  it("appears in our screen", () => {
    const { getByText } = render(<Timer />)

    getByText(/\d/)
  })

  it('does a countdown', () => {
    jest.useFakeTimers()
    const { getByText } = render(<Timer />)

    getByText(/10/)

    act(() => {
      jest.advanceTimersByTime(1000)
    })

    getByText(/9/)
  })

  xit('fires a function after the timeout', () => {
    jest.useFakeTimers()
    const mockFunction = jest.fn()
    const { getByText } = render(<Timer runOutOfTime={mockFunction} />)

    act(() => {
      jest.advanceTimersByTime(10000)
    })

    expect(mockFunction).toHaveBeenCalled()
  })
})


