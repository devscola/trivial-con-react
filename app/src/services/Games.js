import "regenerator-runtime/runtime"
import { get, post } from './HTTPRequester'

class Games {
  async saveGame(gameInfo) {
    const data = await post(`/saveGame`, gameInfo)
    return data
  }

  saveQuestion(ourQuestion) {
    post(`/saveQuestion`, ourQuestion)
  }

  async selectCategory(category) {
    const data = await get(`/retrieveGames?category=${category}`)
    return data
  }

 async retrieveQuestions(gameName){
   const data = await get(`/retrieveQuestions?gameName=${gameName}`)
   return data
  }
}

export default Games