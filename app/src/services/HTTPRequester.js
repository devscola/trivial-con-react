export async function get(url) {
  const response = await fetch(`${process.env.API_URL}${url}`)
  const result = await response.json()
  return result
}

export async function post(url, payload) {

  const postFeatures = {
    method: 'POST',
    body: JSON.stringify(payload),
    headers: {
      'Content-Type': 'application/json'
    }
  }
  const response = await fetch(`${process.env.API_URL}${url}`, postFeatures)
  const result = await response.json()
  return result
}