import React from 'react'
import Button from './Button'

function ErrorManagerModal({show, handleClose, messageContent}) {
    
    const errorColor = {
        position: 'absolute',
        top: '30%',
        left: '30%',
        right: '30%',
        bottom: '30%',
        color: 'white',
        backgroundColor: 'red'
      }
      const close = 'close'
      
    if (show == true){
        return(
            <div className="modal is-active" >
                <div className="modal-background"></div>
                <div className="modal-content message is-medium is-warning">
                    <div className="message-header">
                        <p>Error</p>
                        <Button className="delete is-medium" aria-label="close" handleClick={handleClose}/>
                    </div>
                    <div className="message-body">
                        {messageContent}
                    </div>
                </div>
                <Button className="modal-close is-large" aria-label="close" handleClick={handleClose}/>
            </div>
        )
    }
    return null

}

export default ErrorManagerModal