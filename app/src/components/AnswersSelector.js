import React from "react"

function AnswersSelector({saveChosenAnswer, answersList}) {
    const handleChange = (event) => {
        saveChosenAnswer(event.target.value)
    }

    const printAnswers = () => {

        return answersList.map((answer) =>
            <span key={answer}>
                <input
                    onChange={e => handleChange(e)}
                    type='radio'
                    name='answerOption'
                    value={answer}
                />
                {answer}
                <br />
            </span>)
    }

    return (
        <div id='answers'>
            {printAnswers()}
        </div>
    )
}

export default AnswersSelector
