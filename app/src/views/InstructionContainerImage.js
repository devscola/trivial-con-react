
import React from 'react'
function InstructionContainerImage({imageUrl,imageDescription,bContent,pContent}){
    return (
        <li>
            <img src={imageUrl} alt={imageDescription}/>
            <p>
                <b>{bContent}</b>
                {pContent}
            </p>
        </li>
    )

}
export default InstructionContainerImage