import React, { useState } from "react";
import Button from "../../components/Button"
import Games from '../../services/Games'

const game = new Games()

function SelectCategory({ userName, changeView, changeGameName }) {
  const [games, setGames] = useState([])

  const showCategoryName = async (category) => {
    const response = await game.selectCategory(category)
    if (response.status =="error") {
      return alert(response.message)
    }
    setGames(response.data[0].name)

  }

  return (
    <div className="select-category-container">
      <h1 className='title is-1'>DevTrivia</h1>
      <div className="columns is-centered select-category">
        <div className="categoryColumn column">
          <p className='title is-4 is-centered mb-3 has-text-center mb-5'>{userName}, choose the game's category to play:</p>
          <Button buttonName=" Languages &amp; frameworks " handleClick={() => showCategoryName("Languages and frameworks")} className="button  is-primary is-outlined is-light mb-3 mt-2 " />
          <Button buttonName="Tools and libraries" handleClick={() => showCategoryName("Tools and libraries")} className="button is-warning is-outlined is-light mb-3" />
          <Button buttonName="Methodologies" handleClick={() => showCategoryName("Methodologies")} className="button is-link is-outlined is-light mb-3 " />
          <Button buttonName="Women coding" handleClick={() => showCategoryName("Women coding")} className="button is-danger is-outlined is-light mb-3 " />
          <Button buttonName="Devscola" handleClick={() => showCategoryName("Devscola")} className="button is-info is-outlined is-light mb-3 " />
          <Button buttonName="Curiosities" handleClick={() => showCategoryName("Curiosities")} className="button is-success is-outlined is-light mb-3 " />
        </div>
        <div className='gamesColumn card column mt-5'>
          {games.map((game, index) => {
            return (<span className='tag is-info' key={index} onClick={() => { changeView("GameManager"), changeGameName(game) }}>{game}</span>)
          })}
        </div>
      </div>
      <Button className='button is-primary is-medium has-text-weight-bold mt-5' buttonName="Back" handleClick={() => changeView("Home")} />
    </div>
  )

}




export default SelectCategory