import React, { useState, useEffect } from "react"

import Button from "../../components/Button"
import AnswersSelector from "../../components/AnswersSelector"
import Timer from '../../components/Timer'
import Games from '../../services/Games'

const game = new Games()

const FULL_POINTS_PER_QUESTION = 1
const NO_POINTS = 0

function GameScreen({questionIndex, changeResultScreenValues, userName, changeView,gameName}){

  const [question, setQuestion] = useState("")
  const [answers, setAnswers] = useState([])
  const [correctAnswer, setCorrectAnswer] = useState("")
  const [chosenAnswer, setChosenAnswer] = useState("")
  
  useEffect(() => {
    getQuestion()
  }, [])

  const getQuestion = async () => {
  const response = await game.retrieveQuestions(gameName)
      
      if(response.data.length <= questionIndex){
       
         return changeView('EndGame')
      }
      setQuestion(response.data[questionIndex].question)
      setAnswers(response.data[questionIndex].answers)
      setCorrectAnswer(response.data[questionIndex].correctAnswer)
   
  }
 
  const postAnswer = (points) => {
    let userPoints = {userName: userName, points: points}

    fetch(`${process.env.API_URL}/userPoints`, {
      method: 'POST',
      body: JSON.stringify(userPoints),
      headers: {
        'Accept':'application/json',
        'Content-Type': 'application/json'
      }
    }
    ).then(response=>response.json())
    .catch(error=> alert("error"))
  }

  const evaluateAnswer = () => {
    let [resultSentence, points] = answerIsCorrect()
    postAnswer(points)
    changeResultScreenValues(resultSentence, points, correctAnswer, chosenAnswer)
  }

  const answerIsCorrect = () => {
    if(correctAnswer === chosenAnswer){
      return ["You were right!", FULL_POINTS_PER_QUESTION]
    }
    return ["You were wrong!", NO_POINTS]
  }

  const saveChosenAnswer = (event) => {
    setChosenAnswer(event.target.value)
  }

  return (
    <div className="gameScreen-container">
      <h1 className="title is-1">Trivial Game</h1>
      <Timer className="title is-3" runOutOfTime={() => evaluateAnswer()}/>
      <h4 className="title is-4">{question}</h4>
      <AnswersSelector
        answersList={answers}
        saveChosenAnswer={() => saveChosenAnswer(event)}
      />
      <Button className="button is-primary" handleClick={() => evaluateAnswer()} buttonName="Send" />
    </div>
  )
}

export default GameScreen
