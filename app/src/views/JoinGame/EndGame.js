import React from "react";
import Button from "../../components/Button"

function EndGame({ userName, changeView, totalPoints }) {

  return (
    <div>
        <h3 className="title is-3">Thank you for playing</h3>
        <h3>{userName} your total points are:{totalPoints}</h3>
        <Button handleClick={() => changeView('Home')} buttonName="Back to home" className="button is-primary"/>  
    </div>
  )
}

export default EndGame;
