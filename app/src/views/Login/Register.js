import React, { useState } from "react"
import ErrorManagerModal from "../../components/ErrorManagerModal"
import Users from "../../services/Users"

const user = new Users()

function Register({ changeView, nameChanged }) {
  const [userName, setUserName] = useState("")
  const [password, setPassword] = useState("")
  const [correctPassword, setCorrectPassword] = useState("")
  const [existingUserError, setExistingUserError] = useState(false)
  const [registerError, setRegisterError] = useState(false)

  const meetsRequirements = () => {
    if (!(userName.length >= 6) || !(password.length >= 6) || !(password === correctPassword)) {
      return setRegisterError(true)
    }
    return true
  }
  const check = async () => {
    if (meetsRequirements()) {
      const userAndPassword = { userName: userName, password: password }
      const data = await user.registerNewUser(userAndPassword)

      if (data.status == "success") {
        nameChanged(userName)
        changeView("Home")
      } else {
        setExistingUserError(true)
      }
    }
  }
  return (
    <div className="register-container">
      <h1 className="title is-1">Register</h1>
      <input type="text" className="input is-primary is-medium is-rounded mb-5" placeholder="username" onChange={(event) => setUserName(event.target.value)} value={userName}></input>
      <input type="password" className="input is-primary is-medium is-rounded mb-5" placeholder="password" type="password" onChange={(event) => setPassword(event.target.value)} value={password}></input>
      <input type="password" className="input is-primary is-medium is-rounded mb-5" placeholder="repeat password" onChange={(event) => setCorrectPassword(event.target.value)} value={correctPassword}></input>
      <div className="buttons button-container">
        <button className="button is-primary is-medium has-text-weight-bold mt-5" onClick={() => check()}>
          Create new user
        </button>
        <button className="button is-light is-medium has-text-weight-bold has-text-primary mt-5" onClick={() => changeView("Identifyer")}>
          Back
        </button>
        <ErrorManagerModal show={existingUserError} handleClose={() => setExistingUserError(false)} messageContent={"This username is already taken"} />
        <ErrorManagerModal show={registerError} handleClose={() => setRegisterError(false)} messageContent={"Make sure every field has at least 6 characters and your password matches"} />
      </div>
    </div>
  )
}

export default Register