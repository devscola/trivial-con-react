import React, { useState } from "react"
import ErrorManagerModal from "../../components/ErrorManagerModal"
import Button from "../../components/Button"
import Users from "../../services/Users"

const users = new Users()

function Identifyer({ nameChanged, changeView }) {
  const [name, setName] = useState("")
  const [password, setPassword] = useState("")
  const [errorMessage, setErrorMessage] = useState("Error 404 :(")
  const [errorWithUser, setErrorWithUser] = useState(false)

  const handleNameChange = (event) => {
    setName(event.target.value)
  }
  const handlePasswordChange = (event) => {
    setPassword(event.target.value)
  }

  const handleSubmit = () => {
    if (name.length > 5 && password.length > 5) {
      postUser()
    } else {
      setErrorMessage("You need a user name")
      setErrorWithUser(true)
    }
  }

  const postUser = async () => {
    const user = { userName: name, password: password }
    const response = await users.login(user)

    if (!response.messageError) {
      nameChanged(name)
      changeView("Home")
    } else {
      setErrorMessage(response.messageError)
      setErrorWithUser(true)
    }
  }

  return (
    <div className="identifyer-container">
      <h2 className="title is-2 mt-0">DEVSCOLA'S GAME</h2>
      <h1 className="title is-1">Please log in</h1>
      <input className="input is-rounded is-medium" placeholder={"Choose a cool name"} type="text" onChange={handleNameChange} value={name}></input> <br />
      <input className="input is-rounded is-medium" placeholder={"Enter your password"} type="password" onChange={handlePasswordChange} value={password}></input> <br />
      <div className="button-container">
        <Button className="button is-primary is-medium has-text-weight-bold" handleClick={handleSubmit} buttonName="Sign in" />
        <Button
          className="button is-primary is-medium has-text-weight-bold ml-3"
          handleClick={() => {
            changeView("Register")
          }}
          buttonName="Register"
        />
      </div>
      <ErrorManagerModal show={errorWithUser} handleClose={() => setErrorWithUser(false)} messageContent={errorMessage} />
    </div>
  )
}

export default Identifyer
