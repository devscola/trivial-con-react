import React from 'react'
import InstructionContainerImage from './InstructionContainerImage'
import '../../asset/css/howToStyle.css'
import img1 from '../../asset/img/buildNewGame/question.png'
import img2 from '../../asset/img/buildNewGame/maximum_answers.png'
import img3 from '../../asset/img/buildNewGame/select_answer.png'
import img4 from '../../asset/img/buildNewGame/click_send_question.png'
import img5 from '../../asset/img/buildNewGame/second_question.png'
import Button from "../components/Button"


function Instructions({changeView}) {
    return (
        <div class="instructions-container">
            <Button
                className="button is-info"
                handleClick={() => changeView("Home")}
                buttonName="Back to home"
              />
            <article>
                <h1>Welcome to Trivia Game!</h1>
                <section id="buildNewGame">
                    <h3>** Build a New Game</h3>
                    <p className="description">You can add questions to the Trivia game when you click 
                    on the BuildNewGame button</p>
                    <ul>
                        <InstructionContainerImage
                            imageUrl={img1}
                            imageDescription="Trivial Game Instructions Image. Show how to create a question"
                            bContent="Write the question in the box under question 1"
                            pContent="The question cannot be sent until you create the answer options
                            and select what is the right answer."/>
                        <InstructionContainerImage
                            imageUrl={img2}
                            imageDescription="Trivial Game Instructions Image.
                            Shows how to create a question with your answer options"
                            bContent="Write the answers options"
                            pContent="By pressing the Add button, you can add a new empty box to insert
                            a new answer option"/>
                        <InstructionContainerImage
                            imageUrl={img3}
                            imageDescription="Trivial Game Instructions Image. Shows how to select the
                            correct answer"
                            bContent="Select, by clicking on the corresponding circle, the answer option
                            right"
                            pContent="You can create a minimum of two response options and a maximum of four."/>
                        <InstructionContainerImage
                            imageUrl={img4}
                            imageDescription="Trivial Game Instructions Image. 
                            Show the new question screen,once we have pressed the Send question button"
                            bContent="Clicking the Send question button"
                            pContent="Clicking the Send question button sends your question with 
                            its answers to the game Trivia and a new question appears to complete"/>
                        <InstructionContainerImage
                            imageUrl={img5}
                            imageDescription="Trivial Game Instructions Image.
                            Shows the creation of a correct question"
                            bContent="Image of a right question, just before pressing the Send Question button"
                            pContent="To complete it, write question, from 2 to 4 answer options and select
                            the right answer."/>
                        <InstructionContainerImage
                            imageUrl={img5}
                            imageDescription="Trivial Game Instructions Image.
                            Displays the initial screen with buttons and instructions"
                            bContent="Trivial game main screen"
                            pContent="If once you have sent the last question, you no longer want to continue 
                            writing questions, press the Done button and it will take you to the Home screen."/>
                    </ul>
                </section>
                <section id="joinGame">
                    <h3>** Join Game</h3>
                    <p className="description">When you click on the Join Game button, the game starts</p>
                    <ul>
                        <InstructionContainerImage
                            imageUrl={`${require (`../../asset/img/joinGame/join_game_view.png`)}`}
                            imageDescription="Trivial Game Image.Show the first question of the game."
                            bContent="First question with possible answer options."
                            pContent="You can send your response by clicking the Send button.
                            If the time is up, the reply is sent automatically"/>
                        <InstructionContainerImage
                            imageUrl={`${require (`../../asset/img/joinGame/correct_answer_time.png`)}`}
                            imageDescription="Trivial Game Image.Shows an image with the selected answer."
                            bContent="Select your answer by clicking on the circle in the
                            option that you consider to be the right one.."
                            pContent="By clicking the Send Button, it will show you the answers
                            and the scores screen.You can check in the next image."/>
                        <InstructionContainerImage
                            imageUrl={`${require (`../../asset/img/joinGame/correct_answers.png`)}`}
                            imageDescription="Game Trivial Game image. Display screen response and scores."
                            bContent="Correct answer screen and scores"
                            pContent=" When the time set on the score screen ends,the next question is loaded."/>   
                    </ul>
                    <Button
                        className={"button is-info mb-5"}
                        handleClick={() => changeView("Home")}
                        buttonName="Back to home"
              />
                </section>
            </article>
        </div>
    )
}

export default Instructions