import React, { useState } from "react";

import Button from "../../components/Button"
import Games from '../../services/Games'
import ErrorManagerModal from '../../components/ErrorManagerModal'

const GAMES = new Games()

function AddGameInfo({ changeView, changeGameName }) {
  const [gameName, setGameName] = useState("")
  const [category, setCategory] = useState("")
  const [style, setStyle] = useState(["is-light", "is-light", "is-light", "is-light", "is-light", "is-light"])

  const [errorEmptyInfo, setErrorEmptyInfo] = useState(false)
  const [errorMessage, setErrorMessage] = useState(false)

  const handleChange = (event) => {
    setGameName(event.target.value)
  }

  const handleCategoryChange = (selectedCategory) => {

    setCategory(selectedCategory)
    let categories = ["Languages and frameworks", "Tools and libraries", "Methodologies", "Women coding", "Devscola", "Curiosities"]
    for (let item of categories) {
      let appearance = style
      let index = categories.indexOf(item)
      if (item != selectedCategory) {
        appearance[index] = "is-light"
      }
      if (item == selectedCategory) {
        appearance[index] = ""
      }
      setStyle(appearance)
    }
  }

  const saveGame = async () => {
    const gameInfo = {
      name: gameName,
      category: category,
    }
    const response = await GAMES.saveGame(gameInfo)
    if (response.status == 'fail') {
      setErrorMessage(response.data.message)
      setErrorEmptyInfo(true)
      return
    }
    changeView("BuildGame")
    changeGameName([gameName])
  }
  const handleClick = () => {
    if (gameName && category) {
      saveGame()
      return
    }
    setErrorMessage("You need a category and a game name ;(")
    setErrorEmptyInfo(true)
  }
  return (
    <div className="container pl-5">
      <h1 className="title is-1" >Trivial Game</h1>
      <h3 className="title">Game Name:</h3>
      <input onChange={handleChange}  className=" game input is-rounded mr-5  mt-1 mb-3" type="text" placeholder="Write the name here"></input>

      <h3 className="title  mt-3 mb-5">Choose the game's category:</h3>
      <div className="buttons">
        <Button buttonName="Languages and frameworks" handleClick={() => handleCategoryChange("Languages and frameworks")} className={`button mr-4 mb-3 is-medium is-primary ${style[0]}`} />
        <Button buttonName="Tools and libraries" handleClick={() => handleCategoryChange("Tools and libraries")} className={`button mr-4 mb-3 is-medium is-warning ${style[1]}`} />
        <Button buttonName="Methodologies" handleClick={() => handleCategoryChange("Methodologies")} className={`button mr-4 mb-3 is-medium is-link ${style[2]}`} />
        <Button buttonName="Women coding" handleClick={() => handleCategoryChange("Women coding")} className={`button mr-4 mb-3 is-medium is-danger ${style[3]}`} />
        <Button buttonName="Devscola" handleClick={() => handleCategoryChange("Devscola")} className={`button mr-4 mb-3 is-medium is-info ${style[4]}`} />
        <Button buttonName="Curiosities" handleClick={() => handleCategoryChange("Curiosities")} className={`button is-medium mb-3 is-success ${style[5]}`} />
      </div>
      <div className="buttons is-right mt-2" >
        <Button className='button is-primary is-medium has-text-weight-bold mr-5 mt-3 is-right' buttonName="Create" handleClick={handleClick} />
        <Button className='button is-light is-medium has-text-weight-bold  mt-3 is-right' buttonName="Back" handleClick={() => changeView("Home")} />
      </div>
      <ErrorManagerModal show={errorEmptyInfo} handleClose={() => setErrorEmptyInfo(false)} messageContent={errorMessage} />

    </div>
//button is-light is-medium has-text-weight-bold has-text-primary mt-3 is-right
  )
}

export default AddGameInfo