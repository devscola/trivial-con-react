# React Trivial by DEVSCOLA

This application is make to learn Javascript and React

## Table of Contents

- [React Trivial by DEVSCOLA](#react-trivial-by-devscola)
  - [Table of Contents](#table-of-contents)
  - [System requeriments](#system-requeriments)
  - [How to run the application](#how-to-run-the-application)
  - [How to run Unitary tests](#how-to-run-unitary-tests)
    - [E2E tests in console](#e2e-tests-in-console)
  - [How to open cypress in console](#how-to-open-cypress-in-console)
  - [Deploy demo](#deploy-demo)

## System requeriments

- Docker-compose version 1.24.1.


## How to run the application

- Run: `docker-compose up --build`.
- Open in browser: 
  - APP: `localhost:1234`.
  - API: `localhost:8081`.

## How to run  Unitary tests

- Unitary test if docker-compose is up: `docker-compose exec app npm run test`.
- Unitary test if docker-compose is down: `docker-compose run --rm app npm run test`.
- Do the same for Api test.

## How to run Acceptance test

  ### E2E tests in console
    - Run: 
      `cd e2e
      docker run -v $(pwd):/app -w /app node:alpine npm install
      cd ..
      docker run --network="host" -v $(pwd)/e2e:/workdir rdelafuente/cypress
      `.
    - or:  
      - ./run-all-test.sh



## Deploy demo

Currently, we have a  deploy  application for demo purposes, you can check it out at:

- Heroku Cloud Application Plataform:

  - [APP](https://trivial-react-devscola.herokuapp.com/ "Complete Aplication").
  - [API](https://trivial-react-api.herokuapp.com/ "Only Api").

![Heroku: Cloud Application Platform](https://dab1nmslvvntp.cloudfront.net/wp-content/uploads/2016/04/1461122387heroku-logo.jpg)
